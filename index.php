<?php 
	//Auto-loading all files from composer
	require 'vendor/autoload.php';

	//Debug - remove in production or read from config files
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	ini_set('display_startup_errors', 1);

	//Getting Request & Response objects from PSR Message
	use \Psr\Http\Message\ServerRequestInterface as Request;
  	use \Psr\Http\Message\ResponseInterface as Response;

  	//Creating a new Slim3 application, change setting in production.
  	$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);

  	//Adding PDO in container to be accesible everywhere
  	$c = $app->getContainer();
  	$c['db'] = function($c){
		$pdo = new PDO("mysql:host=localhost;dbname=boilerplate", "root","");
    	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    	return $pdo;
  	};

  	//Including the user class
  	include "classes/user.php";

  	//Returns a json encoded error response object to be sent further
  	function sendError($code, $message, $res){
	    $response['error'] = true;
	    $response['error_code'] = $code;
	    $response['message'] = $message;
	    return $res->withJson($response);
  	}

  	//returns a json encoded success response object to be sent further
  	function sendSuccess($message, $data, $res){
	    $response['error'] = false;
	    $response['message'] = $message;
	    $response['data'] = $data;
	    return $res->withJson($response);
  	}

  	//Function to check if all required data is present in a request
	function checkRequired($requiredFields, $req){
		$error = false;
		$request = $req->getParsedBody();
		foreach ($requiredFields as $field) {
		  if(isset($request[$field])){
		    continue;
		  }else{
		    $error = true;
		  }
		}
		return $error;
	}

	//Function to authenticate incoming requests using token, fetch user data from token
	$authorizeToken = function($request, $response, $next){
		$token = $request->getHeader('authorization');
		if(strlen($token[0]) != 0){
		  $user = new User($this->db);
		  $details = $user->initFromToken($token[0]);
		  if($details){
		    $request = $request->withAttribute('user', $details);
		    $response = $next($request, $response);
		    return $response;
		  }else{
		    return $response->withJson($details);
		  }
		}else{
		  return $response->withJson("Authentication Required");
		}
	};


  	//Public: Create a new user and return a token
  	$app->post('/auth/signup', function(Request $req, Response $res){
    	$required_fields = array("username", "email", "password");
    	$error = checkRequired($required_fields, $req);
    	if($error){
    		return sendError(1, "Insufficient data", $res);
    	}else{
    		$user = new User($this->db);
    		$data = $req->getParsedBody();
    		$id = $user->createNew($data['username'], $data['email'], $data['password']);
    		if($id){
    			return sendSuccess("User created succesfully", $id, $res);
    		}else{
    			return sendError(2, "Invalid username or email", $res);
    		}
    	}
  	});

  	//Public: Check for existing user and create a session, returns a token
  	$app->post('/auth/login', function(Request $req, Response $res){
    	$required_fields = array("username", "password");
    	$error = checkRequired($required_fields, $req);
    	if($error){
    		return sendError(1, "Insufficient data", $res);
    	}else{
    		$user = new User($this->db);
    		$data = $req->getParsedBody();
    		$result = $user->login($data['username'], $data['password']);
    		if($result){
    			return sendSuccess("login success", $result, $res);
    		}else{
    			return sendError(3, "User login Failed", $res);
    		}
    		
    	}
  	});

  	//Protected: send access_token header, removes a session and logout user
    $app->get('/auth/logout', function(Request $req, Response $res){
      $data = $req->getAttribute('user');
      $user = new User($this->db);
      $result = $user->logout($data['id']);
      return sendSuccess("User logged out", true, $res);
    })->add($authorizeToken);

  	$app->run();
?>