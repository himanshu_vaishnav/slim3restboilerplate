<?php 
	/**
	* user class stores a user object and provides 
	* methods to interact with a user object.
	*/
	class User
	{
		
		private $con;
		private $user;
		private $status;

		function __construct($db)
		{
			$this->con = $db;
			$this->status = "Un-Initiated";
		}

		//Get status of a user
		public function getStatus(){
			return $this->status;
		}

		//Initiate a user with session
		public function initFromToken($token){
			$q = "SELECT * FROM `users` WHERE `token` = :token";
			$stmt = $this->con->prepare($q);
			$stmt->execute(array(":token" => $token));
			$result = $stmt->fetch();
			if($result){
				$this->user = $result;
				return $this->user;
			}else{
				return $result;
			}
		}


		//create a new user in database, returns a user_id
		public function createNew($username, $email, $password){
			try{
				$q = "INSERT INTO `users` (`username`, `email`, `pass`) VALUES (:username, :email, :pass)";
				$stmt = $this->con->prepare($q);
				$bindings = array(":username" => $username, ":email" => $email, ":pass" => $password);
				$rows = $stmt->execute($bindings);
				return array('id' => $this->con->lastInsertId());
			}catch(PDOException $e){
				return false;
			}
		}

		//validates the credentials and return an access token to user
		public function login($username, $password){
			$q = "SELECT * FROM `users` WHERE `username` = :username";
			$stmt = $this->con->prepare($q);
			$stmt->execute(array(":username"=>$username));
			$data = $stmt->fetch();

			if($data){
				if($data['pass'] === $password){
					if(isset($data['token']) && $data['token'] == ""){
						return $data;
					}else{
						$token = bin2hex(openssl_random_pseudo_bytes(8));
						$q = "UPDATE `users` SET `token` = :token WHERE `id` = :id";
						$stmt = $this->con->prepare($q);
						$rows = $stmt->execute(array(':token'=>$token, ':id'=>$data['id']));
						$data['token'] = $token;
						return $data;
					}
				}else{
					//Invalid user, wrong password
					return false;
				}
			}else{
				//No user with this username 
				return false;
			}
		}

		//logs out the user by deleting the token, can be regenerated using login method
		public function logout($id){
			$q = "UPDATE `users` SET `token` = NULL WHERE `id` = :id";
			$stmt = $this->con->prepare($q);
			return $stmt->execute(array(':id'=>$id));
		}
	}
?>